package main

import (
	"gitlab.com/socialspecters.io/specters/internal/app/specters-cli/cmd"
)

// Version is the version of docdb-console
var Version = "No Version Provided"

func main() {
	err := cmd.Execute(Version)
	if err != nil {
		cmd.Fatal(err)
	}
}
