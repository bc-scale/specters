package pkg

import (
	"context"
	"crypto/ed25519"
	"crypto/rand"
	"fmt"
	"github.com/golang-jwt/jwt"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/stretchr/testify/assert"
	"gitlab.com/socialspecters.io/specters/pkg/api/client"
	"gitlab.com/socialspecters.io/specters/pkg/api/pb"
	"gitlab.com/socialspecters.io/specters/pkg/api/server"
	"gitlab.com/socialspecters.io/specters/pkg/db"
	"gitlab.com/socialspecters.io/specters/pkg/specters"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/test/bufconn"
	"log"
	"net"
	"testing"
)

const bufSize = 1024 * 1024

var (
	lis     *bufconn.Listener
	key     crypto.PrivKey
	manager *specters.Specters
)

type MockDB struct{}

func NewMockDB() db.DB {
	return &MockDB{}
}

func (m *MockDB) Close() error {
	return nil
}

func (m *MockDB) CreateSpecters(context.Context, db.InstanceDB) error {
	return nil
}

func (m *MockDB) GetSpecter(pubKey string, specter db.InstanceDB, publicKey db.PublicKeyDB) (db.InstanceDB, error) {
	return specters.NewSpecterDB("CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C")
}

func (m *MockDB) HasSpecters() (bool, error) {
	return true, nil
}

func (m *MockDB) CreateSpecter(db.InstanceDB, db.PrivateKeyDB) (db.InstanceID, error) {
	return "", nil
}

func init() {
	lis = bufconn.Listen(bufSize)
	privKey, _, err := crypto.GenerateEd25519Key(rand.Reader)
	key = privKey
	if err != nil {
		log.Fatalf("Error generating server key: %v", err)
	}
	manager, err = specters.NewSpecters(context.Background(), "", "", NewMockDB(), specters.Config{Debug: false})
	manager.SetIdentity(specters.NewLibP2PIdentity(key))
	if err != nil {
		log.Fatalf("Error creating Specters manager: %v", err)
	}
	apiServer, _ := server.NewServer(manager, server.Config{Debug: false})

	s := grpc.NewServer(
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			server.StreamStatusInterceptor(manager),
		)),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			server.UnaryStatusInterceptor(manager),
		)),
	)

	pb.RegisterAPIServer(s, apiServer)
	go func() {
		if err = s.Serve(lis); err != nil {
			log.Fatalf("Server exited with error: %v", err)
		}
	}()
}

func bufDialer(context.Context, string) (net.Conn, error) {
	return lis.Dial()
}

func TestClient_Status(t *testing.T) {
	tests := []struct {
		name         string
		expectingMsg string
		expectingErr bool
	}{
		{
			name:         "All success no error",
			expectingErr: false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			ctx := context.Background()
			c, err := client.NewClient(ctx, "bufnet", grpc.WithContextDialer(bufDialer), grpc.WithTransportCredentials(insecure.NewCredentials()))
			if err != nil {
				tt.Fatalf("Failed to dial bufnet: %v", err)
			}
			defer func(c *client.Client) {
				err = c.Close()
				if err != nil {
				}
			}(c)

			reply, err := c.Status(ctx)
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist, err)
			assert.Equal(tt, "", reply.ClusterID)
			assert.Equal(tt, "", reply.Version)
			assert.True(tt, reply.Initialized)
			assert.False(tt, reply.Sealed)
			assert.Equal(tt, int32(specters.ShamirParts), reply.Shares)
			assert.Equal(tt, int32(specters.ShamirThreshold), reply.Threshold)
		})
	}
}

func TestClient_Init(t *testing.T) {
	tests := []struct {
		name         string
		expectingErr bool
	}{
		{
			name:         "All success no error",
			expectingErr: false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			manager.SetIdentity(nil)
			defer func() { manager.SetIdentity(specters.NewLibP2PIdentity(key)) }()

			ctx := context.Background()
			c, err := client.NewClient(ctx, "bufnet", grpc.WithContextDialer(bufDialer), grpc.WithTransportCredentials(insecure.NewCredentials()))
			if err != nil {
				tt.Fatalf("Failed to dial bufnet: %v", err)
			}
			defer func(c *client.Client) {
				err = c.Close()
				if err != nil {
				}
			}(c)

			_, pubKey, err := crypto.GenerateEd25519Key(rand.Reader)
			if err != nil {
				t.Fatalf("Error creating root key")
			}
			pk, _ := specters.NewLibP2PPubKey(pubKey).MarshalBinary()

			reply, err := c.Init(ctx, pk)
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist, err)
			assert.Equal(tt, specters.ShamirParts, len(reply.Keys))
			assert.Equal(tt, int32(specters.ShamirThreshold), reply.Threshold)
		})
	}
}

func TestClient_Unseal(t *testing.T) {
	tests := []struct {
		name         string
		expectingErr bool
	}{
		{
			name:         "All success no error",
			expectingErr: false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			manager.SetIdentity(nil)
			defer func() { manager.SetIdentity(specters.NewLibP2PIdentity(key)) }()

			_, pubKey, err := crypto.GenerateEd25519Key(rand.Reader)
			if err != nil {
				t.Fatalf("Error creating root key")
			}
			pk, _ := specters.NewLibP2PPubKey(pubKey).MarshalBinary()
			parts, err := manager.Init(pk)
			if err != nil {
				tt.Fatalf("Failed to dial bufnet: %v", err)
			}

			ctx := context.Background()
			c, err := client.NewClient(ctx, "bufnet", grpc.WithContextDialer(bufDialer), grpc.WithTransportCredentials(insecure.NewCredentials()))
			if err != nil {
				tt.Fatalf("Failed to dial bufnet: %v", err)
			}
			defer func(c *client.Client) {
				err = c.Close()
				if err != nil {
				}
			}(c)

			reply, err := c.Unseal(ctx, parts[0])
			errExist := err != nil
			assert.Equal(tt, tc.expectingErr, errExist, err)
			assert.Equal(tt, "", reply.ClusterID)
			assert.Equal(tt, "", reply.Version)
			assert.True(tt, reply.Initialized)
			assert.True(tt, reply.Sealed)
			assert.Equal(tt, int32(specters.ShamirParts), reply.Shares)
			assert.Equal(tt, int32(specters.ShamirThreshold), reply.Threshold)
			assert.Equal(tt, int32(1), reply.Progress)

			reply, err = c.Unseal(ctx, parts[1])
			errExist = err != nil
			assert.Equal(tt, "", reply.ClusterID)
			assert.Equal(tt, "", reply.Version)
			assert.True(tt, reply.Initialized)
			assert.True(tt, reply.Sealed)
			assert.Equal(tt, int32(specters.ShamirParts), reply.Shares)
			assert.Equal(tt, int32(specters.ShamirThreshold), reply.Threshold)
			assert.Equal(tt, int32(2), reply.Progress)

			reply, err = c.Unseal(ctx, parts[2])
			errExist = err != nil
			assert.Equal(tt, "", reply.ClusterID)
			assert.Equal(tt, "", reply.Version)
			assert.True(tt, reply.Initialized)
			assert.False(tt, reply.Sealed)
			assert.Equal(tt, int32(specters.ShamirParts), reply.Shares)
			assert.Equal(tt, int32(specters.ShamirThreshold), reply.Threshold)
			assert.Equal(tt, int32(3), reply.Progress)
		})
	}
}

func TestClient_GetToken(t *testing.T) {
	tests := []struct {
		name           string
		identity       string
		expectingToken string
		expectingErr   bool
	}{
		{
			name:           "All success no error",
			identity:       "CAESQPtFAFQMwikaJT5odV++QDsjJuGLeJbvpzbAMQwUKUgclfHXXbBAOy62afjjIM/rIyuVkZ7BG9TRTDp/3T1S7gI=",
			expectingToken: "token",
			expectingErr:   false,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			ctx := context.Background()
			c, err := client.NewClient(ctx, "bufnet", grpc.WithContextDialer(bufDialer), grpc.WithTransportCredentials(insecure.NewCredentials()))
			if err != nil {
				tt.Fatalf("Failed to dial bufnet: %v", err)
			}
			defer func(c *client.Client) {
				err = c.Close()
				if err != nil {
				}
			}(c)

			decodedIdentity, err := specters.DecodeKey(tc.identity)
			if err != nil {
				tt.Fatal("Error decoding identity")
			}

			identity, err := specters.UnmarshalLibP2PIdentity(decodedIdentity)
			if err != nil {
				tt.Fatal("Error unmarshalling identity")
			}

			tokenString, err := c.GetToken(ctx, identity)
			errExist := err != nil

			pubKeyRaw, _ := key.GetPublic().Raw()
			token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodEd25519); !ok {
					return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
				}

				return ed25519.PublicKey(pubKeyRaw), nil
			})
			if err != nil {
				tt.Fatalf("Error parsing token: %v", err)
			}

			assert.Equal(tt, tc.expectingErr, errExist, err)

			if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
				assert.Equal(tt, "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C", claims["sub"])
				assert.Equal(tt, specters.EncodeKey(pubKeyRaw), claims["iss"])
				assert.NotNil(tt, claims["iat"])
			} else {
				tt.Fatalf("Error getting JWT claims: %v", err)
			}
		})
	}
}
