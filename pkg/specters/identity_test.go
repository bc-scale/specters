package specters

import (
	"crypto/rand"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewLibP2PIdentity(t *testing.T) {
	tests := []struct {
		name string
	}{
		{
			name: "All success no error",
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			privKey, _, err := crypto.GenerateEd25519Key(rand.Reader)
			if err != nil {
				tt.Fatalf("Error generating private key: %v", err)
			}

			identity := NewLibP2PIdentity(privKey)
			bytes, err := identity.MarshalBinary()
			if err != nil {
				tt.Fatalf("Error marshalling private key: %v", err)
			}

			assert.NotNil(tt, identity)
			assert.NotNil(tt, bytes)
		})
	}
}

func TestUnmarshalLibP2PIdentity(t *testing.T) {
	tests := []struct {
		name         string
		identity     string
		expectingErr bool
	}{
		{
			name:         "All success no error",
			identity:     "CAESQPtFAFQMwikaJT5odV++QDsjJuGLeJbvpzbAMQwUKUgclfHXXbBAOy62afjjIM/rIyuVkZ7BG9TRTDp/3T1S7gI=",
			expectingErr: false,
		},
		{
			name:         "Error unmarshalling entity",
			identity:     "",
			expectingErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			decodedIdentity, err := DecodeKey(tc.identity)
			if err != nil {
				tt.Fatal("Error decoding identity")
			}

			_, err = UnmarshalLibP2PIdentity(decodedIdentity)
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist)
		})
	}
}

func TestNewLibP2PPubKey(t *testing.T) {
	tests := []struct {
		name string
	}{
		{
			name: "All success no error",
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			privKey, _, err := crypto.GenerateEd25519Key(rand.Reader)
			if err != nil {
				tt.Fatalf("Error generating public key: %v", err)
			}

			identity := NewLibP2PPubKey(privKey.GetPublic())
			bytes, err := identity.MarshalBinary()
			if err != nil {
				tt.Fatalf("Error marshalling public key: %v", err)
			}

			assert.NotNil(tt, identity)
			assert.NotNil(tt, bytes)
		})
	}
}

func TestUnmarshalLibP2PPubKey(t *testing.T) {
	tests := []struct {
		name         string
		pubKey       string
		expectingErr bool
	}{
		{
			name:         "All success no error",
			pubKey:       "CAESIJXx112wQDsutmn44yDP6yMrlZGewRvU0Uw6f909Uu4C",
			expectingErr: false,
		},
		{
			name:         "Error unmarshalling public key",
			pubKey:       "",
			expectingErr: true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			decodedPubKey, err := DecodeKey(tc.pubKey)
			if err != nil {
				tt.Fatal("Error decoding public key")
			}

			_, err = UnmarshalLibP2PPubKey(decodedPubKey)
			errExist := err != nil

			assert.Equal(tt, tc.expectingErr, errExist)
		})
	}
}

func TestLibP2PIdentity_SignVerify(t *testing.T) {
	tests := []struct {
		name            string
		identity        string
		data            string
		expectingSigned string
	}{
		{
			name:     "All success no error",
			identity: "CAESQPtFAFQMwikaJT5odV++QDsjJuGLeJbvpzbAMQwUKUgclfHXXbBAOy62afjjIM/rIyuVkZ7BG9TRTDp/3T1S7gI=",
			data:     "data-to-sign",
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			decodedIdentity, err := DecodeKey(tc.identity)
			if err != nil {
				tt.Fatal("Error decoding identity")
			}

			identity, err := UnmarshalLibP2PIdentity(decodedIdentity)
			if err != nil {
				tt.Fatal("Error unmarshalling identity")
			}

			signedData, err := identity.Sign([]byte(tc.data))
			if err != nil {
				tt.Fatal("Error signing data")
			}

			valid, err := identity.GetPublic().Verify([]byte(tc.data), signedData)
			if err != nil {
				tt.Fatal("Error verifying data")
			}

			assert.True(tt, valid)
		})
	}
}

func TestEncodeKey(t *testing.T) {
	tests := []struct {
		name     string
		identity string
	}{
		{
			name:     "All success no error",
			identity: "CAESQPtFAFQMwikaJT5odV++QDsjJuGLeJbvpzbAMQwUKUgclfHXXbBAOy62afjjIM/rIyuVkZ7BG9TRTDp/3T1S7gI=",
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(tt *testing.T) {
			decodedIdentity, err := DecodeKey(tc.identity)
			if err != nil {
				tt.Fatal("Error decoding identity")
			}
			encodedIdentity := EncodeKey(decodedIdentity)

			assert.Equal(tt, tc.identity, encodedIdentity)
		})
	}
}
