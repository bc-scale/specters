package specters

import (
	"encoding"
	"encoding/base64"
	"github.com/libp2p/go-libp2p-core/crypto"
	"gitlab.com/socialspecters.io/specters/pkg/db"
)

// Identity represents an entity capable of signing a message
// and returning the associated public key for verification.
type Identity interface {
	encoding.BinaryMarshaler
	encoding.BinaryUnmarshaler

	db.PrivateKeyDB

	// Raw returns the raw bytes of the key (not wrapped in the
	// libp2p-crypto protobuf).
	//
	// This function is the inverse of {Priv,Pub}KeyUnmarshaler.
	Raw() ([]byte, error)

	// Sign the given bytes cryptographically.
	Sign([]byte) ([]byte, error)

	// GetPublic returns the public key paired with this identity.
	GetPublic() PubKey
}

// LibP2PIdentity wraps crypto.PrivKey, overwriting GetPublic with specters.PubKey.
type LibP2PIdentity struct {
	crypto.PrivKey
}

// NewLibP2PIdentity returns a new LibP2PIdentity.
func NewLibP2PIdentity(key crypto.PrivKey) Identity {
	return &LibP2PIdentity{PrivKey: key}
}

// UnmarshalLibP2PIdentity converts a protobuf serialized LibP2PIdentity into its
// representative object
func UnmarshalLibP2PIdentity(data []byte) (Identity, error) {
	identity := &LibP2PIdentity{}
	err := identity.UnmarshalBinary(data)
	if err != nil {
		return nil, err
	}

	return identity, nil
}

// MarshalBinary converts an identity object into its protobuf serialized form.
func (p *LibP2PIdentity) MarshalBinary() ([]byte, error) {
	return crypto.MarshalPrivateKey(p.PrivKey)
}

// UnmarshalBinary converts a protobuf serialized identity into its
// representative object
func (p *LibP2PIdentity) UnmarshalBinary(data []byte) error {
	privKey, err := crypto.UnmarshalPrivateKey(data)
	if err != nil {
		return err
	}

	p.PrivKey = privKey
	return nil
}

// Raw returns the raw bytes of the key (not wrapped in the
// libp2p-crypto protobuf).
//
// This function is the inverse of {Priv,Pub}KeyUnmarshaler.
func (p *LibP2PIdentity) Raw() ([]byte, error) {
	return p.PrivKey.Raw()
}

// Sign the given bytes cryptographically.
func (p *LibP2PIdentity) Sign(data []byte) ([]byte, error) {
	return p.PrivKey.Sign(data)
}

// GetPublic returns the public key paired with this identity.
func (p *LibP2PIdentity) GetPublic() PubKey {
	return NewLibP2PPubKey(p.PrivKey.GetPublic())
}

// GetPublicDB returns the public key DB paired with this identity.
func (p *LibP2PIdentity) GetPublicDB() db.PublicKeyDB {
	return p.PrivKey.GetPublic()
}

// PubKey can be anything that provides a verify method.
type PubKey interface {
	encoding.BinaryMarshaler
	encoding.BinaryUnmarshaler

	// Raw returns the raw bytes of the key (not wrapped in the
	// libp2p-crypto protobuf).
	//
	// This function is the inverse of {Priv,Pub}KeyUnmarshaler.
	Raw() ([]byte, error)

	// Verify that 'sig' is the signed hash of 'data'
	Verify([]byte, []byte) (bool, error)
}

// LibP2PPubKey wraps crypto.PubKey.
type LibP2PPubKey struct {
	crypto.PubKey
}

// NewLibP2PPubKey returns a new PubKey.
func NewLibP2PPubKey(key crypto.PubKey) PubKey {
	return &LibP2PPubKey{PubKey: key}
}

// UnmarshalLibP2PPubKey converts a protobuf serialized LibP2PPubKey into its
// representative object
func UnmarshalLibP2PPubKey(data []byte) (PubKey, error) {
	pubKey := &LibP2PPubKey{}
	err := pubKey.UnmarshalBinary(data)
	if err != nil {
		return nil, err
	}

	return pubKey, nil
}

// MarshalBinary converts an identity object into its protobuf serialized form.
func (p *LibP2PPubKey) MarshalBinary() ([]byte, error) {
	return crypto.MarshalPublicKey(p.PubKey)
}

// UnmarshalBinary converts a protobuf serialized identity into its
// representative object
func (p *LibP2PPubKey) UnmarshalBinary(data []byte) error {
	pubKey, err := crypto.UnmarshalPublicKey(data)
	if err != nil {
		return err
	}

	p.PubKey = pubKey
	return nil
}

// Raw returns the raw bytes of the key (not wrapped in the
// libp2p-crypto protobuf).
//
// This function is the inverse of {Priv,Pub}KeyUnmarshaler.
func (p *LibP2PPubKey) Raw() ([]byte, error) {
	return p.PubKey.Raw()
}

// Verify that 'sig' is the signed hash of 'data'
func (p *LibP2PPubKey) Verify(data []byte, sigBytes []byte) (bool, error) {
	return p.PubKey.Verify(data, sigBytes)
}

// DecodeKey decodes from b64 to a byte array that can be unmarshalled.
func DecodeKey(b string) ([]byte, error) {
	return base64.StdEncoding.DecodeString(b)
}

// EncodeKey encodes a marshalled key to b64.
func EncodeKey(b []byte) string {
	return base64.StdEncoding.EncodeToString(b)
}
