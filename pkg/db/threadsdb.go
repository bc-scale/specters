package db

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/alecthomas/jsonschema"
	logging "github.com/ipfs/go-log/v2"
	"github.com/textileio/go-threads/core/app"
	"github.com/textileio/go-threads/core/thread"
	"github.com/textileio/go-threads/db"
	"github.com/textileio/go-threads/db/keytransform"
	"gitlab.com/socialspecters.io/specters/pkg/util"
	"reflect"
)

var (
	loggingSetLogLevel = logging.SetLogLevel
	log                = logging.Logger("specters:db")
)

// ThreadsDB is an implementation of Db using Textile Threads
type ThreadsDB struct {
	manager  *db.Manager
	specters *db.DB
}

// NewThreadsDB creates a new instance of ThreadsDB
func NewThreadsDB(store keytransform.TxnDatastoreExtended, network app.Net, config Config) (DB, error) {
	err := loggingSetLogLevel("specters:db", util.LevelFromDebugFlag(config.Debug))
	if err != nil {
		return nil, err
	}

	manager, err := db.NewManager(store, network, db.WithNewDebug(config.Debug))
	if err != nil {
		return nil, err
	}

	return &ThreadsDB{
		manager: manager,
	}, nil
}

// Close closes DB
func (t *ThreadsDB) Close() error {
	log.Debug("received close")
	return t.manager.Close()
}

// CreateSpecters creates the Specters DB if it doesn't exist
func (t *ThreadsDB) CreateSpecters(ctx context.Context, specter InstanceDB) error {
	log.Info("loading or creating specters DB")

	dbs, err := t.manager.ListDBs(ctx)
	if err != nil {
		return nil
	}

	for id, d := range dbs {
		dbInfo, err := d.GetDBInfo()
		if err != nil {
			return err
		}
		if dbInfo.Name == dbName {
			t.specters = d
			log.Debugf("got db %s", id)
			log.Info("finished loading or creating specters DB")
			return nil
		}
	}

	reflector := jsonschema.Reflector{}
	mySchema := reflector.Reflect(specter)

	id := thread.NewIDV1(thread.Raw, 32)
	d, err := t.manager.NewDB(
		ctx,
		id,
		db.WithNewManagedName(dbName),
		db.WithNewManagedCollections(db.CollectionConfig{
			Name:   "Specters",
			Schema: mySchema,
			Indexes: []db.Index{{
				Path:   "public_key",
				Unique: true,
			}},
		}),
	)
	if err != nil {
		return err
	}
	t.specters = d

	log.Debugf("created db %s", id)
	log.Infof("finished loading or creating specters DB %s", id)
	return nil
}

// GetSpecter gets a Specter from Specters DB
func (t *ThreadsDB) GetSpecter(pubKey string, specter InstanceDB, publicKey PublicKeyDB) (InstanceDB, error) {
	coll := t.specters.GetCollection("Specters")
	query := db.Where("public_key").Eq(pubKey)
	instances, err := coll.Find(query)
	if err != nil {
		return nil, err
	}
	if len(instances) == 0 {
		return nil, nil
	}

	result, err := processFindReply(instances, specter, true)
	if err != nil {
		return nil, err
	}

	instanceDB := result.(InstanceDB)

	valid, err := instanceDB.VerifyInstance(publicKey)
	if err != nil {
		return nil, err
	}
	if !valid {
		return nil, fmt.Errorf("failed verifing specter signature")
	}

	return instanceDB, nil
}

// HasSpecters returns if at least one specter exists
func (t *ThreadsDB) HasSpecters() (bool, error) {
	coll := t.specters.GetCollection("Specters")
	query := db.OrderBy("_id").LimitTo(1)
	instances, err := coll.Find(query)
	if err != nil {
		return false, err
	}
	return len(instances) == 1, nil
}

// CreateSpecter creates a Specter in Specters DB
func (t *ThreadsDB) CreateSpecter(specter InstanceDB, privateKey PrivateKeyDB) (InstanceID, error) {
	bytes, err := json.Marshal(specter)
	if err != nil {
		return "", err
	}

	coll := t.specters.GetCollection("Specters")
	threadsID, err := coll.Create(bytes)
	if err != nil {
		return "", err
	}

	specter.SetID(InstanceID(threadsID))
	err = specter.SignInstance(privateKey)
	if err != nil {
		return "", err
	}

	bytes, err = json.Marshal(specter)
	if err != nil {
		return "", err
	}

	err = coll.Save(bytes)
	if err != nil {
		return "", err
	}

	return InstanceID(threadsID), nil
}

func processFindReply(instances [][]byte, dummy interface{}, onlyOne bool) (interface{}, error) {
	sliceType := reflect.TypeOf(dummy)
	elementType := sliceType.Elem()
	length := len(instances)
	results := reflect.MakeSlice(reflect.SliceOf(sliceType), length, length)
	for i, result := range instances {
		target := reflect.New(elementType).Interface()
		err := json.Unmarshal(result, target)
		if err != nil {
			return nil, err
		}
		val := results.Index(i)
		val.Set(reflect.ValueOf(target))
	}

	if onlyOne {
		return results.Index(0).Interface(), nil
	}

	return results.Interface(), nil
}
