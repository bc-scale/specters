package cmd

import (
	"fmt"
	"runtime"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of specters-cli",
	Long:  `All software has versions. This is specters-cli's`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf(`specters-cli %s, Compiler: %s %s, Copyright (C) 2022 Social Specters.`,
			version,
			runtime.Compiler,
			runtime.Version())
		fmt.Println()
	},
}
